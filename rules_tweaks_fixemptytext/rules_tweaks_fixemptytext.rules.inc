<?php

/**
 * @file
 * rules_tweaks_fixemptytext.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_fixemptytext_rules_action_info() {
  $actions = array(
    'rules_tweaks_fixemptytext_fix' => array(
      'label' => t('Fix empty text'),
      'group' => t('Rules Tweaks'),
      'parameter' => array(
        'text' => array(
          'type' => 'text',
          'label' => t('Text to fix'),
          'allow null' => TRUE,
          'optional' => TRUE,
        ),
      ),
      'provides' => array(
        'fixed' => array(
          'type' => 'text',
          'label' => t('Fixed text'),
          'description' => t('If the original text was NULL (maybe from empty text input), this will be an empty string.')
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Action callback: Fix.
 */
function rules_tweaks_fixemptytext_fix($text) {
  if (is_null($text)) {
    $text = '';
  }
  return array('fixed' => $text);
}
