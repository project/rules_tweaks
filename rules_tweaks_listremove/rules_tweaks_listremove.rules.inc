<?php

/**
 * @file
 * rules_tweaks_emptylist.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_listremove_rules_action_info() {
  $return['rules_tweaks_listremove_action'] = array(
    'label' => t('Remove an item by index from a list (without autosave)'),
    'group' => t('Rules Tweaks'),
    'parameter' => array(
      'list' => array(
        'type' => 'list',
        'label' => t('List', array(), array('context' => 'data_types')),
        'description' => t('The data list for which an item is to be removed.'),
        'restriction' => 'selector',
        //'save' => TRUE,
      ),
      'index' => array(
        'type' => 'integer',
        'label' => t('Index of the item to remove'),
        'description' => t('Use index 0 for the first list element. Use negative index to count from end, so -1 is last element.'),
      ),
    ),
  );
  return $return;
}

/**
 * Action callback: Remove item.
 */
function rules_tweaks_listremove_action($list, $index) {
  // Make sure the list is indexed numerically.
  $list = array_values($list);
  if ($index < 0) {
    $index = count($list) + $index;
  }
  if (isset($list[$index])) {
    unset($list[$index]);
    $list = array_values($list);
  }
  return array('list' => $list);
}
