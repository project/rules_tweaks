<?php

/**
 * @file
 * rules_tweaks_entitymark.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_entitymark_rules_action_info() {
  $common = rules_tweaks_entitymark_common_info();
  $common_parameters = rules_tweaks_entitymark_common_parameters();
  $actions['rules_tweaks_entitymark_action_set_mark'] = $common + array(
    'label' => t('Set entity mark'),
    'group' => t('Rules Tweaks'),
    'parameter' => $common_parameters + array(
      'value' => array(
        'type' => 'unknown',
        'label' => t('Value of the mark'),
        'wrapped' => TRUE,
        'default mode' => 'input',
      ),
    ),
  );

  $actions['rules_tweaks_entitymark_action_get_mark'] = $common + array(
    'label' => t('Get entitiy mark'),
    'group' => t('Rules Tweaks'),
    'parameter' => $common_parameters,
    'provides' => array(
      'value' => array(
        'type' => 'unknown',
        'label' => t('Value of the mark'),
      ),
    ),
  );
  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function rules_tweaks_entitymark_rules_condition_info() {
  $common = rules_tweaks_entitymark_common_info();
  $common_parameters = rules_tweaks_entitymark_common_parameters();
  $conditions['rules_tweaks_entitymark_condition_mark_is_empty'] = $common + array(
    'label' => t('Entity mark is empty'),
    'parameter' => $common_parameters,
  );
  return $conditions;
}

/**
 * Info for all actions and conditions.
 * @return array
 */
function rules_tweaks_entitymark_common_info() {
  return array(
    'group' => t('Rules Tweaks'),
    'callbacks' => array(
      'form_alter' => 'rules_tweaks_entitymark_type_form_alter',
      'validate' => 'rules_tweaks_entitymark_type_validate',
      'info_alter' => 'rules_tweaks_entitymark_type_info_alter',
    ),
  );
}

/**
 * Parameters for all actions and conditions.
 * @return array
 */
function rules_tweaks_entitymark_common_parameters() {
  return array(
    'entity' => array(
      'type' => 'entity',
      'label' => t('Entity'),
      'wrapped' => TRUE, // Rules wraps 'entity' type anyway.
      'allow null' => TRUE,
    ),
    'name' => array(
      'type' => 'text',
      'label' => t('Name'),
    ),
    'type' => array(
      'type' => 'text',
      'label' => t('Data type'),
      'options list' => 'rules_tweaks_entitymark_data_type_options',
    ),
  );
}

/**
 * Common info alter callback.
 */
function rules_tweaks_entitymark_type_info_alter(&$element_info, RulesAbstractPlugin $element) {
  if (isset($element->settings['type']) && $type = $element->settings['type']) {
    if (isset($element_info['parameter']['value'])) {
      $element_info['parameter']['value']['type'] = $type;
    }
    if (isset($element_info['provides']['value'])) {
      $element_info['provides']['value']['type'] = $type;
    }
  }
}

/**
 * Custom validate callback.
 */
function rules_tweaks_entitymark_type_validate($element) {
  if (!isset($element->settings['type'])) {
    throw new RulesIntegrityException(t('Invalid type specified.'), array($element, 'parameter', 'type'));
  }
}

/**
 * Action callback: Set entity mark.
 *
 * @param string $type
 * @param \EntityDrupalWrapper $entity_wrapper
 * @param string $name
 * @param mixed $value
 */
function rules_tweaks_entitymark_action_set_mark(\EntityDrupalWrapper $entity_wrapper, $name, $type, $value) {
  $entity = $entity_wrapper->value();
  if (!$entity) {
    return;
  }
  if (!isset($entity->_rules_tweaks_entitymark)) {
    $entity->_rules_tweaks_entitymark = array();
  }
  $entity->_rules_tweaks_entitymark[$type][$name] = $value;
}

/**
 * Action callback: Get entity mark.
 *
 * @param string $type
 * @param object $entity
 * @param string $name
 * @return mixed
 */
function rules_tweaks_entitymark_action_get_mark($entity, $name, $type) {
  $wrapper = rules_tweaks_entitymark_get_mark($type, $entity, $name);
  return array('value' => $wrapper);
}

/**
 * Get entity mark (internal).
 *
 * @param $type
 * @param \EntityDrupalWrapper $entity_wrapper
 * @param string $name
 * @return mixed
 *   We requested wrapped date in the setter action, but mind that rules does
 *   not wrap scalar types.
 */
function rules_tweaks_entitymark_get_mark($type, \EntityDrupalWrapper $entity_wrapper, $name) {
  $entity = $entity_wrapper->value();
  if (isset($entity) && isset($entity->_rules_tweaks_entitymark[$type][$name])) {
    return $entity->_rules_tweaks_entitymark[$type][$name];
  }
  else {
    return NULL;
  }
}

/**
 * Condition callback: Entity is marked.
 *
 * @param object $entity
 * @param string $name
 */
function rules_tweaks_entitymark_condition_mark_is_empty($entity, $name, $type) {
  $wrapper = rules_tweaks_entitymark_get_mark($type, $entity, $name);
  if ($wrapper instanceof EntityMetadataWrapper) {
    $value = $wrapper->value();
  }
  else {
    $value = $wrapper;
  }
  return empty($value);
}

/**
 * Options list callback: Data types.
 */
function rules_tweaks_entitymark_data_type_options() {
  return RulesPluginUI::getOptions('data');
}

/**
 * Form alter callback for actions relying on the entity type or the data type.
 *
 * Copied from @see rules_action_type_form_alter().
 */
function rules_tweaks_entitymark_type_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  $first_step = empty($element->settings['type']);
  $form['reload'] = array(
    '#weight' => 5,
    '#type' => 'submit',
    '#name' => 'reload',
    '#value' => $first_step ? t('Continue') : t('Reload form'),
    '#limit_validation_errors' => array(array('parameter', 'type')),
    '#submit' => array('rules_action_type_form_submit_rebuild'),
    '#ajax' => rules_ui_form_default_ajax(),
  );
  // Use ajax and trigger as the reload button.
  $form['parameter']['type']['settings']['type']['#ajax'] = $form['reload']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload'),
    );

  if ($first_step) {
    // In the first step show only the type select.
    foreach (element_children($form['parameter']) as $key) {
      if ($key != 'type') {
        unset($form['parameter'][$key]);
      }
    }
    unset($form['submit']);
    unset($form['provides']);
    // Disable #ajax for the first step as it has troubles with lazy-loaded JS.
    // @todo: Re-enable once JS lazy-loading is fixed in core.
    unset($form['parameter']['type']['settings']['type']['#ajax']);
    unset($form['reload']['#ajax']);
  }
  else {
    // Hide the reload button in case js is enabled and it's not the first step.
    $form['reload']['#attributes'] = array('class' => array('rules-hide-js'));
  }
}
