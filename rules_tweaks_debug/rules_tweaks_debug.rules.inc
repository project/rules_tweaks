<?php

/**
 * @file
 * rules_tweaks_debug.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_debug_rules_action_info() {
  $return['rules_tweaks_debug'] = array(
    'label' => t('Get rules backtrace'),
    'named parameter' => TRUE,
    'provides' => array(
      'rules_backtrace' => array(
        'type' => 'text',
        'label' => t('Rules backtrace'),
      ),
    ),
    'group' => t('Rules Tweaks'),
  );
  return $return;
}

/**
 * Action callback.
 */
function rules_tweaks_debug() {
  return array('rules_backtrace' => rules_tweaks_debug_text());
}
